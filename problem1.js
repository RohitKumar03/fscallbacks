const fs = require("fs");

/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/


const createFile = (filePath, filename) => {
  fs.writeFile(filePath, JSON.stringify(filename), "utf-8", (error) => {
    if (error) {
      console.log(error);
    }
    let deletefile = `${filePath}`;
    setTimeout(() => {
      fs.rm(deletefile, (error) => {
        console.log(error);
      });
    }, 3000);
  });
};

module.exports = { createFile };
