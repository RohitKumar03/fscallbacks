/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences.
         Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");
const { dirname } = require("path");
const lipsum = "./data/lipsum.txt";

const getUpperCase = (textString) => {
    if(typeof textString !=='string'){
        throw new Error('Invalid input type')
    }
    return textString.toUpperCase();
  };
  const getLowerCase = (upperCaseString) => {
    if(typeof upperCaseString !=='string'){
        throw new Error('Invalid input type')
    }
    return upperCaseString.toLowerCase();
  };
  const getSortString = (lowerCaseString) => {
      if(! Array.isArray(lowerCaseString)){
          throw new Error('Invalid input type')
      }
    return lowerCaseString.sort();
  };


const callBacksOperations = (filename) => {
   
  fs.readFile(filename, "utf-8", (error, lipsumData) => {
    if (error) {
      console.log(error);
    }
    let upperCase = getUpperCase(lipsumData);
    fs.writeFile(`${__dirname}/./output/upperCase.txt`, upperCase, "utf-8", (error) => {
      if (error) {
        console.log(error);
      }
      fs.writeFile(
        `${__dirname}/./output/fileNames.txt`,"upperCase.txt","utf-8",
        (error) => {
          if (error) {
            console.log(error);
          }
          fs.readFile(`${__dirname}/./output/upperCase.txt`,"utf-8",
            (error, upperCaseData) => {
              if (error) {
                console.log(error);
              }
              // console.log(upperCaseData);
              let lowerCase = getLowerCase(upperCaseData);
              let splitSentence = lowerCase.split(".").join("\n");
              //  console.log(splitSentence);
              fs.writeFile(`${__dirname}/./output/lowerCase.txt`,splitSentence,"utf-8",
                (error) => {
                  if (error) {
                    console.log(error);
                  }
                  fs.appendFile(`${__dirname}/./output/fileNames.txt`,"\nlowerCase.txt","utf-8",
                    (error) => {
                      if (error) {
                        console.log(error);
                      }
                      fs.readFile( `${__dirname}/./output/lowerCase.txt`, "utf-8",
                        (error, lowerCaseData) => {
                          if (error) {
                            console.log(error);
                          }
                          let lowerCase = lowerCaseData.trim().split("\n");
                        //    console.log(lowerCase);
                          let sortString = getSortString(lowerCase).join("\n")
                        //   console.log(sortString);
                          fs.writeFile(`${__dirname}/./output/sortString.txt`,sortString, "utf-8",
                            (error) => {
                              if (error) {
                                console.log(error);
                              }
                              fs.appendFile(`${__dirname}/./output/fileNames.txt`,"\nsortString.txt","utf-8",
                                (error) => {
                                  if (error) {
                                    console.log(error);
                                  }
                                  fs.readFile(`${__dirname}/./output/fileNames.txt`,"utf-8",
                                    (error, files) => {
                                      if (error) {
                                        console.log(error);
                                      }
                                      let fileNames = files.split("\n");
                                      setTimeout(() => {
                                        fileNames.forEach((file) => {
                                          fs.rm(`${__dirname}/./output/` + file,(error)=>{
                                              if(error){
                                                  console.log(error);
                                              }
                                          });
                                        });
                                      }, 6 * 1000);
                                    }
                                  );
                                }
                              );
                            }
                          );
                        }
                      );
                    }
                  );
                }
              );
            }
          );
        }
      );
    });
  });
};
// callBacksOperations(lipsum)

  module.exports = {callBacksOperations}

